#include "application.h"
#include "global.h"

Application* Application::a = nullptr;

Application::Application(int argc, char *argv[])
    : running(false)
{
    UNUSED(argc);
    UNUSED(argv);

    // TODO: argc, argv processing


    SDL_Init(SDL_INIT_VIDEO);
}

Application::~Application()
{
    SDL_Quit();
}

int Application::exec()
{
    // create event loop here.
    running = true;

    while (running) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT)
            {
                running = false;
            }
        }
    }

    return 0;
}

Application *Application::app()
{
    return a;
}
