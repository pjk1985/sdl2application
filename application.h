#ifndef APPLICATION_H
#define APPLICATION_H


class Application
{
public:
    Application(int argc, char *argv[]);
    virtual ~Application();

    int exec();

    static Application * app();

private:
    static Application * a;
    bool running;
};

#endif // APPLICATION_H
