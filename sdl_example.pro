TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += file_copies
COPIES += depends

SOURCES += \
        application.cpp \
        event.cpp \
        main.cpp \
        sdlwindow.cpp \
        window.cpp

INCLUDEPATH += $$PWD/x86_64-w64-mingw32/include
DEPENDPATH += $$PWD/x86_64-w64-mingw32/include

LIBS += \
    -lmingw32 \
    -L$$PWD/x86_64-w64-mingw32/lib/ -lSDL2main \
    -L$$PWD/x86_64-w64-mingw32/lib/ -lSDL2

depends.files += $$files($$PWD/x86_64-w64-mingw32/bin/*.dll)
depends.path = $$OUT_PWD

HEADERS += \
    application.h \
    event.h \
    global.h \
    sdlwindow.h \
    window.h
