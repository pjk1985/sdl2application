#ifndef WINDOW_H
#define WINDOW_H

class Window
{
public:
    Window();
    virtual ~Window();
    virtual void createPlatformWindow() = 0;
    virtual void show() = 0;
    virtual void hide() = 0;
    virtual bool isVisible() const = 0;
};

#endif // WINDOW_H
