#include "application.h"
#include "sdlwindow.h"

int main(int argc, char *argv[])
{
    Application app(argc, argv);

    SDLWindow window;

    window.createPlatformWindow();

    window.show();

    return app.exec();
}
