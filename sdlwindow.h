#ifndef SDLWINDOW_H
#define SDLWINDOW_H

#include "window.h"

struct SDL_Window;

class SDLWindow : public Window
{
public:
    SDLWindow();
    virtual ~SDLWindow()override;

    void createPlatformWindow() override;

    void show() override;
    void hide() override;

    bool isVisible() const override;

private:
    SDL_Window *window;
    bool visible;
};

#endif // SDLWINDOW_H
