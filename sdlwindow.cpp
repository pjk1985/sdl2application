#include "sdlwindow.h"
#include "global.h"

SDLWindow::SDLWindow()
    :window(nullptr)
    ,visible(false)
{

}

SDLWindow::~SDLWindow()
{
    if(window){
        SDL_DestroyWindow(window);
    }
}

void SDLWindow::createPlatformWindow()
{
    // Create an application window with the following settings:
    window = SDL_CreateWindow(
        "",                  // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        640,                               // width, in pixels
        480,                               // height, in pixels
        SDL_WINDOW_OPENGL                  // flags - see below
    );

    // Check that the window was successfully created
    if (window == nullptr) {
        // In the case that the window could not be made...
        std::cout << "Could not create window: " << SDL_GetError() << std::endl;
        exit(0);
    }

    hide();
}

void SDLWindow::show()
{
    assert(window);
    SDL_ShowWindow(window);
    if(!visible){
        visible = true;
    }
}

void SDLWindow::hide()
{
    assert(window);
    SDL_HideWindow(window);
    if(visible){
        visible = false;
    }
}

bool SDLWindow::isVisible() const
{
    return visible;
}
