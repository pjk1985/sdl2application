#ifndef GLOBAL_H
#define GLOBAL_H

// this SDL Platform
#include <SDL2/SDL.h>

#include <iostream>
#include <cassert>

#define UNUSED(x) (void)(x)

#endif // GLOBAL_H
